package com.example.akbarsintegrationservice.repositories;

import com.example.akbarsintegrationservice.request_models.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestRepository extends JpaRepository<Request, Long> {
    List<Request> findByInn(String inn);
}

package com.example.akbarsintegrationservice.services;

import com.example.akbarsintegrationservice.focus_api.ConnectionStatus;
import org.hibernate.JDBCException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;

public interface IntegrationServiceInterface {
    Long saveDownInn(String inn) throws JDBCException, SQLException;
    Long queryData(Long id) throws SQLException, IOException;
    ConnectionStatus getConnectionStatus();
}

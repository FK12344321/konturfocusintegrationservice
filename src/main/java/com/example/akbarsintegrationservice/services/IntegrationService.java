package com.example.akbarsintegrationservice.services;

import com.example.akbarsintegrationservice.focus_api.ConnectionStatus;
import com.example.akbarsintegrationservice.focus_api.FocusAPICaller;
import com.example.akbarsintegrationservice.focus_api.FocusAPICallerInterface;
import com.example.akbarsintegrationservice.repositories.RequestRepository;
import com.example.akbarsintegrationservice.repositories.ResponseRepository;
import com.example.akbarsintegrationservice.request_models.Request;
import com.example.akbarsintegrationservice.response_models.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

@Service
public class IntegrationService implements IntegrationServiceInterface {
    private final RequestRepository requestRepository;
    private final ResponseRepository responseRepository;
    private final FocusAPICallerInterface focusAPICaller;

    @Autowired
    public IntegrationService(RequestRepository requestRepository,
                              ResponseRepository responseRepository,
                              FocusAPICallerInterface focusAPICaller) {
        this.requestRepository = requestRepository;
        this.responseRepository = responseRepository;
        this.focusAPICaller = focusAPICaller;
    }

    public Long saveDownInn(String inn) throws JDBCException, SQLException {
        Request request = new Request(inn);
        Request savedRequest = requestRepository.save(request);
        return savedRequest.getId();
    }

    public Long queryData(Long requestId) throws SQLException, IOException {
        Request request = getRequestById(requestId);
        String inn = request.getInn();
        String responseJson = focusAPICaller.getDataByInn(inn);
        Long responseId = saveDownResponse(responseJson);
        Response response = responseRepository.getReferenceById(responseId);
        request.setResponse(response);
        requestRepository.save(request);
        return responseId;
    }

    public ConnectionStatus getConnectionStatus() {
        return focusAPICaller.getConnectionStatus();
    }

    private Request getRequestById(Long requestId) throws SQLException {
        try {
            return requestRepository.getReferenceById(requestId);
        }
        catch (EntityNotFoundException e) {
            throw new SQLException("no such id in the system");
        }
    }

    private Long saveDownResponse(String json) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        Response[] response = objectMapper.readValue(json, Response[].class);
        return responseRepository.save(response[0]).getId();
    }
}

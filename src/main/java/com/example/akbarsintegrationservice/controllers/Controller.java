package com.example.akbarsintegrationservice.controllers;

import com.example.akbarsintegrationservice.focus_api.ConnectionStatus;
import com.example.akbarsintegrationservice.services.IntegrationService;
import com.example.akbarsintegrationservice.services.IntegrationServiceInterface;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import jdk.jfr.ContentType;
import org.hibernate.JDBCException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class Controller {
    private final IntegrationServiceInterface integrationService;
    private final Logger logger;

    @Autowired
    public Controller(IntegrationServiceInterface integrationService) {
        this.integrationService = integrationService;
        this.logger = LoggerFactory.getLogger(this.getClass());
    }


    @PostMapping("/inn/{inn}")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Map<String, Object> saveDownInn(@PathVariable String inn) {
        logger.info("Accepted request to save down inn " + inn);
        try {
            if (!InnValidator.isValid(inn))
                throw new IllegalArgumentException();
            Long innId = integrationService.saveDownInn(inn);
            logger.info("Inn " + inn + "is saved down with id = " + innId);
            return createResponseJson("successfully saved down the inn", innId);
        } catch (JDBCException | IllegalArgumentException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "something is wrong with your request"
            );
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(
                    HttpStatus.SERVICE_UNAVAILABLE, "some services are not available"
            );
        }
    }

    @GetMapping("/kontur-focus-connection")
    public Map<String, Object> getConnectionStatus() {
        logger.info("Accepted request to check connection to focus API");
        Map<String, Object> response = new HashMap<>();
        ConnectionStatus connectionStatus = integrationService.getConnectionStatus();
        if (connectionStatus == ConnectionStatus.AVAILABLE) {
            logger.info("Focus API is available");
        } else {
            logger.warn("Focus API is NOT available");
        }
        response.put("status", connectionStatus.toString());
        return response;
    }

    @GetMapping("/inn/info/{id}")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Map<String, Object> getInnInfoById(@PathVariable Long id) {
        logger.info("Accepted request to query data by its id");
        try {
            Long responseId = integrationService.queryData(id);
            logger.info("Information about the inn holder has been saved down");
            return createResponseJson("successfully saved down info", responseId);
        } catch (SQLException e) {
            logger.warn(e.getMessage());
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "something is wrong with the request"
            );
        } catch (IOException e) {
            logger.warn(e.getMessage());
            throw new ResponseStatusException(
                    HttpStatus.SERVICE_UNAVAILABLE, "some services are not available"
            );
        }
    }

    private Map<String, Object> createResponseJson(String status, Long id) {
        Map<String, Object> response = new HashMap<>();
        response.put("status", status);
        response.put("id", id);
        return response;
    }
}

package com.example.akbarsintegrationservice.controllers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InnValidator {
    public static boolean isValid(String inn) {
        String innRegex = "\\d{10}|\\d{12}";
        return inn.matches(innRegex);
    }
}

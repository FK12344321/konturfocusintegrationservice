package com.example.akbarsintegrationservice.request_models;

import com.example.akbarsintegrationservice.response_models.Response;

import javax.persistence.*;

@Entity
@Table(name="IN_KF_REQUEST")
public class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String inn;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "responseId")
    private Response response;

    public Request(String inn) {
        this.inn = inn;
    }

    public Request() {
    }

    public Long getId() {
        return id;
    }

    public String getInn() {
        return inn;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}

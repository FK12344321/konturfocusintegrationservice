package com.example.akbarsintegrationservice.focus_api;

public enum ConnectionStatus {
    AVAILABLE("AVAILABLE"), UNAVAILABLE("UNAVAILABLE");

    private final String status;

    ConnectionStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}

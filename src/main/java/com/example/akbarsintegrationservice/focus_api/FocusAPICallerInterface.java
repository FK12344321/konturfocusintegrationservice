package com.example.akbarsintegrationservice.focus_api;

import java.io.IOException;

public interface FocusAPICallerInterface {
    String getDataByInn(String inn) throws IOException;
    ConnectionStatus getConnectionStatus();
}

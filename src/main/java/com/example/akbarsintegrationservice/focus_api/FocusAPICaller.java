package com.example.akbarsintegrationservice.focus_api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Component
@PropertySource("classpath:focus.properties")
public class FocusAPICaller implements FocusAPICallerInterface {
    private final String accessKey;
    private final String apiUrl;

    public FocusAPICaller(@Value("${focus.api.access-key}") String accessKey,
                          @Value("${focus.api.url}") String apiUrl) {
        this.accessKey = accessKey;
        this.apiUrl = apiUrl;
    }

    public String getDataByInn(String inn) throws IOException {
        URL requestUrl = buildQuery(inn);
        HttpURLConnection connection = (HttpURLConnection) requestUrl.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        connection.disconnect();
        return content.toString();
    }

    public ConnectionStatus getConnectionStatus() {
        try {
            URL serviceUrl = new URL(apiUrl);
            HttpURLConnection connection = (HttpURLConnection) serviceUrl.openConnection();
            connection.disconnect();
            return ConnectionStatus.AVAILABLE;
        }
        catch (Exception exception) {
            return ConnectionStatus.UNAVAILABLE;
        }
    }

    private URL buildQuery(String inn) throws MalformedURLException {
        String url = this.apiUrl + "req?key=" + accessKey + "&inn=" + inn;
        return new URL(url);
    }
}

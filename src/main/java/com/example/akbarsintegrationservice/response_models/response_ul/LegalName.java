package com.example.akbarsintegrationservice.response_models.response_ul;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "OUT_KF_RESPONSE_UL_LEGAL_NAME")
public class LegalName{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "responseUlId")
    private ResponseUl responseUl;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "historyId")
    private History history;

    @JsonProperty("short")
    @Column(name = "short")
    private String myshort;

    @Column(name = "fullName")
    private String full;
    private String readable;
    private String date;

    public LegalName() {
    }

    public Long getId() {
        return id;
    }

    public ResponseUl getResponseUl() {
        return responseUl;
    }

    public History getHistory() {
        return history;
    }

    public String getMyshort() {
        return myshort;
    }

    public String getFull() {
        return full;
    }

    public String getReadable() {
        return readable;
    }

    public String getDate() {
        return date;
    }
}
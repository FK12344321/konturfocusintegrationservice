package com.example.akbarsintegrationservice.response_models.response_ul;

import javax.persistence.*;

@Entity
@Table(name = "OUT_KF_RESPONSE_UL_BRANCH_FOREIGN_ADDRESS")
public class ForeignAddress {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "branchId")
    private Branch branch;

    private String countryName;
    private String addressString;

    public ForeignAddress() {
    }

    public Long getId() {
        return id;
    }

    public Branch getBranch() {
        return branch;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getAddressString() {
        return addressString;
    }
}

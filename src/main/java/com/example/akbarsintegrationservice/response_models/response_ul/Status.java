package com.example.akbarsintegrationservice.response_models.response_ul;

import javax.persistence.*;

@Entity(name = "StatusUl")
@Table(name = "OUT_KF_RESPONSE_UL_STATUS")
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "responseUlId")
    private ResponseUl responseUl;

    private String statusString;
    @Column(nullable = true)
    private Boolean reorganizing;
    @Column(nullable = true)
    private Boolean bankrupting;
    @Column(nullable = true)
    private Boolean dissolving;
    @Column(nullable = true)
    private Boolean dissolved;
    private String date;

    public Status() {
    }

    public Long getId() {
        return id;
    }

    public ResponseUl getResponseUl() {
        return responseUl;
    }

    public String getStatusString() {
        return statusString;
    }

    public Boolean isReorganizing() {
        return reorganizing;
    }

    public Boolean isBankrupting() {
        return bankrupting;
    }

    public Boolean isDissolving() {
        return dissolving;
    }

    public Boolean isDissolved() {
        return dissolved;
    }

    public String getDate() {
        return date;
    }
}

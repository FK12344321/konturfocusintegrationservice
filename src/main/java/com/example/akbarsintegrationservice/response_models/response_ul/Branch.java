package com.example.akbarsintegrationservice.response_models.response_ul;

import javax.persistence.*;

@Entity
@Table(name = "OUT_KF_RESPONSE_UL_BRANCH")
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "responseUlId")
    private ResponseUl responseUl;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "parsedAddressRfId")
    private ParsedAddressRF parsedAddressRF;

    @OneToOne(mappedBy = "branch", cascade = CascadeType.ALL)
    private ForeignAddress foreignAddress;

    private String name;
    private String kpp;
    private String date;

    public Branch() {
    }

    public Long getId() {
        return id;
    }

    public ResponseUl getResponseUl() {
        return responseUl;
    }

    public ParsedAddressRF getParsedAddressRF() {
        return parsedAddressRF;
    }

    public ForeignAddress getForeignAddress() {
        return foreignAddress;
    }

    public String getName() {
        return name;
    }

    public String getKpp() {
        return kpp;
    }

    public String getDate() {
        return date;
    }
}

package com.example.akbarsintegrationservice.response_models.response_ul;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "OUT_KF_RESPONSE_UL_HISTORY")
public class History{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "responseUlId")
    private ResponseUl responseUl;

    @OneToMany(mappedBy = "history", cascade = CascadeType.ALL)
    private Set<Kpp> kpps;

    @OneToMany(mappedBy = "history", cascade = CascadeType.ALL)
    private Set<LegalName> legalNames;

    @OneToMany(mappedBy = "history", cascade = CascadeType.ALL)
    private Set<LegalAddress> legalAddresses;

    @OneToMany(mappedBy = "history", cascade = CascadeType.ALL)
    private Set<Head> heads;

    public History() {
    }

    public Long getId() {
        return id;
    }

    public ResponseUl getResponseUl() {
        return responseUl;
    }

    public Set<Kpp> getKpps() {
        return kpps;
    }

    public Set<LegalName> getLegalNames() {
        return legalNames;
    }

    public Set<LegalAddress> getLegalAddresses() {
        return legalAddresses;
    }

    public Set<Head> getHeads() {
        return heads;
    }
}

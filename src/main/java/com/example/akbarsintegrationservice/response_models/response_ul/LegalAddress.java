package com.example.akbarsintegrationservice.response_models.response_ul;

import javax.persistence.*;

@Entity
@Table(name = "OUT_KF_RESPONSE_UL_LEGAL_ADRESS")
public class LegalAddress{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "responseUlId")
    private ResponseUl responseUl;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "parsedAddressRfId")
    private ParsedAddressRF parsedAddressRF;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "historyId")
    private History history;

    private String date;
    private String firstDate;
    @Column(nullable = true)
    private Boolean isInaccuracy;
    private String inaccuracyDate;

    public LegalAddress() {
    }

    public Long getId() {
        return id;
    }

    public ResponseUl getResponseUl() {
        return responseUl;
    }

    public ParsedAddressRF getParsedAddressRF() {
        return parsedAddressRF;
    }

    public History getHistory() {
        return history;
    }

    public String getDate() {
        return date;
    }

    public String getFirstDate() {
        return firstDate;
    }

    public Boolean getIsInaccuracy() {
        return isInaccuracy;
    }

    public String getInaccuracyDate() {
        return inaccuracyDate;
    }
}

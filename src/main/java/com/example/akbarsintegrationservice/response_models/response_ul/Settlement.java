package com.example.akbarsintegrationservice.response_models.response_ul;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "OUT_KF_RESPONSE_UL_PARSED_ADDRESS_RF_SETTLEMENT")
public class Settlement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String topoShortName;
    private String topoFullName;
    private String topoValue;

    @OneToMany(mappedBy = "regionName", cascade = CascadeType.ALL)
    private Set<ParsedAddressRF> parsedAddressRF;

    public Settlement() {
    }

    public Long getId() {
        return id;
    }

    public Set<ParsedAddressRF> getParsedAddressRF() {
        return parsedAddressRF;
    }

    public String getTopoShortName() {
        return topoShortName;
    }

    public String getTopoFullName() {
        return topoFullName;
    }

    public String getTopoValue() {
        return topoValue;
    }
}

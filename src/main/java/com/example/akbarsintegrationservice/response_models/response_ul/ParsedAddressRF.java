package com.example.akbarsintegrationservice.response_models.response_ul;

import org.springframework.lang.Nullable;

import javax.persistence.*;

@Entity
@Table(name = "OUT_KF_RESPONSE_UL_PARSED_ADDRESS_RF")
public class ParsedAddressRF{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(mappedBy = "parsedAddressRF", cascade = CascadeType.ALL)
    private Branch branch;

    @OneToOne(mappedBy = "parsedAddressRF", cascade = CascadeType.ALL)
    private LegalAddress legalAddress;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "regionNameId")
    private RegionName regionName;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cityNameId")
    private City city;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "settlementNameId")
    private Settlement settlement;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "streetNameId")
    private Street street;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bulkId")
    private Bulk bulk;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "houseId")
    private House house;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "flatId")
    private Flat flat;

    private String zipCode;
    private String kladrCode;
    private String regionCode;
    private String bulkRaw;
    private String houseRaw;
    private String flatRaw;
    @Column(nullable = true)
    private Boolean isConverted;

    public ParsedAddressRF() {
    }

    public Long getId() {
        return id;
    }

    public Branch getBranch() {
        return branch;
    }

    public LegalAddress getLegalAddress() {
        return legalAddress;
    }

    public RegionName getRegionName() {
        return regionName;
    }

    public City getCity() {
        return city;
    }

    public Street getStreet() {
        return street;
    }

    public Bulk getBulk() {
        return bulk;
    }

    public House getHouse() {
        return house;
    }

    public Flat getFlat() {
        return flat;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getKladrCode() {
        return kladrCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public String getBulkRaw() {
        return bulkRaw;
    }

    public String getHouseRaw() {
        return houseRaw;
    }

    public String getFlatRaw() {
        return flatRaw;
    }

    public Boolean getIsConverted() {
        return isConverted;
    }

    public Settlement getSettlement() {
        return settlement;
    }
}

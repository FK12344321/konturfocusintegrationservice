package com.example.akbarsintegrationservice.response_models.response_ul;

import com.example.akbarsintegrationservice.response_models.Response;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "OUT_KF_RESPONSE_UL")
public class ResponseUl {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String kpp;
    private String okpo;
    private String okato;
    private String okfs;
    private String oktmo;
    private String okogu;
    private String okopf;
    private String opf;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "responseId")
    private Response response;

    @OneToOne(mappedBy = "responseUl", cascade = CascadeType.ALL)
    private LegalName legalName;

    @OneToOne(mappedBy = "responseUl", cascade = CascadeType.ALL)
    private LegalAddress legalAddress;

    @OneToMany(mappedBy = "responseUl", cascade = CascadeType.ALL)
    private Set<Branch> branches;

    @OneToOne(mappedBy = "responseUl", cascade = CascadeType.ALL)
    private Status status;

    private String registrationDate;
    private String dissolutionDate;

    @OneToMany(mappedBy = "responseUl", cascade = CascadeType.ALL)
    private Set<Head> heads;

    @OneToMany(mappedBy = "responseUl", cascade = CascadeType.ALL)
    private Set<ManagementCompany> managementCompanies;

    @OneToOne(mappedBy = "responseUl", cascade = CascadeType.ALL)
    private History history;

    public ResponseUl() {
    }

    public Long getId() {
        return id;
    }

    public String getKpp() {
        return kpp;
    }

    public String getOkpo() {
        return okpo;
    }

    public String getOkato() {
        return okato;
    }

    public String getOkfs() {
        return okfs;
    }

    public String getOktmo() {
        return oktmo;
    }

    public String getOkogu() {
        return okogu;
    }

    public String getOkopf() {
        return okopf;
    }

    public String getOpf() {
        return opf;
    }

    public Response getResponse() {
        return response;
    }

    public LegalName getLegalName() {
        return legalName;
    }

    public LegalAddress getLegalAddress() {
        return legalAddress;
    }

    public Set<Branch> getBranches() {
        return branches;
    }

    public Status getStatus() {
        return status;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public String getDissolutionDate() {
        return dissolutionDate;
    }

    public Set<Head> getHeads() {
        return heads;
    }

    public Set<ManagementCompany> getManagementCompanies() {
        return managementCompanies;
    }

    public History getHistory() {
        return history;
    }
}

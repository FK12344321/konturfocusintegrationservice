package com.example.akbarsintegrationservice.response_models.response_ul;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "OUT_KF_RESPONSE_UL_PARSED_ADDRESS_RF_CITY")
public class City{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "regionName", cascade = CascadeType.ALL)
    private Set<ParsedAddressRF> parsedAddressRF;

    private String topoShortName;
    private String topoFullName;
    private String topoValue;

    public City() {
    }

    public Long getId() {
        return id;
    }

    public Set<ParsedAddressRF> getParsedAddressRF() {
        return parsedAddressRF;
    }

    public String getTopoShortName() {
        return topoShortName;
    }

    public String getTopoFullName() {
        return topoFullName;
    }

    public String getTopoValue() {
        return topoValue;
    }
}

package com.example.akbarsintegrationservice.response_models.response_ul;

import javax.persistence.*;

@Entity
@Table(name = "OUT_KF_RESPONSE_UL_HEAD_STUCTURED_FIO")
public class StructuredFio{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "resposneUl")
    private ResponseUl responseUl;

    private String firstName;
    private String lastName;
    private String middleName;

    public StructuredFio() {
    }

    public Long getId() {
        return id;
    }

    public ResponseUl getResponseUl() {
        return responseUl;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }
}

package com.example.akbarsintegrationservice.response_models.response_ul;

import javax.persistence.*;

@Entity
@Table(name = "OUT_KF_RESPONSE_UL_HEAD")
public class Head{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "resposeUlId")
    private ResponseUl responseUl;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "historyId")
    private History history;

    private String fio;
    private String innfl;
    private String position;
    private String date;
    private String firstDate;
    @Column(nullable = true)
    private Boolean isInaccuracy;
    private String inaccuracyDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "responseUlId")
    private StructuredFio structuredFio;

    public Head() {
    }

    public Long getId() {
        return id;
    }

    public ResponseUl getResponseUl() {
        return responseUl;
    }

    public History getHistory() {
        return history;
    }

    public String getFio() {
        return fio;
    }

    public String getInnfl() {
        return innfl;
    }

    public String getPosition() {
        return position;
    }

    public String getDate() {
        return date;
    }

    public String getFirstDate() {
        return firstDate;
    }

    public Boolean getIsInaccuracy() {
        return isInaccuracy;
    }

    public String getInaccuracyDate() {
        return inaccuracyDate;
    }

    public StructuredFio getStructuredFio() {
        return structuredFio;
    }
}

package com.example.akbarsintegrationservice.response_models.response_ul;

import javax.persistence.*;

@Entity
@Table(name = "OUT_KF_RESPONSE_UL_MANAGEMENT_COMPANIES")
public class ManagementCompany{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "responseUlId")
    private ResponseUl responseUl;

    private String name;
    private String inn;
    private String ogrn;
    private String date;
    private String firstDate;
    @Column(nullable = true)
    private Boolean isInaccuracy;
    private String inaccuracyDate;

    public ManagementCompany() {
    }

    public Long getId() {
        return id;
    }

    public ResponseUl getResponseUl() {
        return responseUl;
    }

    public String getName() {
        return name;
    }

    public String getInn() {
        return inn;
    }

    public String getOgrn() {
        return ogrn;
    }

    public String getDate() {
        return date;
    }

    public String getFirstDate() {
        return firstDate;
    }

    public Boolean getIsInaccuracy() {
        return isInaccuracy;
    }

    public String getInaccuracyDate() {
        return inaccuracyDate;
    }
}
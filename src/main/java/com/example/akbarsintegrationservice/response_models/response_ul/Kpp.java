package com.example.akbarsintegrationservice.response_models.response_ul;

import javax.persistence.*;

@Entity
@Table(name = "OUT_KF_RESPONSE_UL_HISTORY_KPP")
public class Kpp{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "historyId")
    private History history;

    private String kpp;
    private String date;

    public Kpp() {
    }

    public Long getId() {
        return id;
    }

    public History getHistory() {
        return history;
    }

    public String getKpp() {
        return kpp;
    }

    public String getDate() {
        return date;
    }
}

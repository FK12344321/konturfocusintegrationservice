package com.example.akbarsintegrationservice.response_models;

import javax.persistence.*;

@Entity
@Table(name = "OUT_KF_RESPONSE_BRIEF_REPORT")
public class BriefReport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(mappedBy = "briefReport", cascade = CascadeType.ALL)
    private Response response;

    @OneToOne(mappedBy = "briefReport", cascade = CascadeType.ALL)
    private Summary summary;

    public BriefReport() {
    }

    public Long getId() {
        return id;
    }

    public Response getResponse() {
        return response;
    }

    public Summary getSummary() {
        return summary;
    }
}

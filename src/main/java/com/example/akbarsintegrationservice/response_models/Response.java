package com.example.akbarsintegrationservice.response_models;

import com.example.akbarsintegrationservice.request_models.Request;
import com.example.akbarsintegrationservice.response_models.response_ip.ResponseIp;
import com.example.akbarsintegrationservice.response_models.response_ul.ResponseUl;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "OUT_KF_RESPONSE")
public class Response {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String inn;
    private String ogrn;
    private String focusHref;

    @JsonProperty("IP")
    @OneToOne(mappedBy = "response", cascade = CascadeType.ALL)
    private ResponseIp ip;

    @JsonProperty("UL")
    @OneToOne(mappedBy = "response", cascade = CascadeType.ALL)
    private ResponseUl ul;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "briefReportId")
    private BriefReport briefReport;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "contactPhonesId")
    private ContactPhones contactPhones;

    @OneToOne(mappedBy = "response", cascade = CascadeType.ALL)
    private Request request;

    public Response() {
    }

    public Long getId() {
        return id;
    }

    public String getInn() {
        return inn;
    }

    public String getOgrn() {
        return ogrn;
    }

    public String getFocusHref() {
        return focusHref;
    }

    public ResponseIp getIp() {
        return ip;
    }

    public ResponseUl getUl() {
        return ul;
    }

    public BriefReport getBriefReport() {
        return briefReport;
    }

    public ContactPhones getContactPhones() {
        return contactPhones;
    }
}

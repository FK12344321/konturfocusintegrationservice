package com.example.akbarsintegrationservice.response_models.response_ip;

import com.example.akbarsintegrationservice.response_models.Response;

import javax.persistence.*;

@Entity
@Table(name="OUT_KF_RESPONSE_IP")
public class ResponseIp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fio;
    private String okpo;
    private String okato;
    private String okfs;
    private String okogu;
    private String okopf;
    private String opf;
    private String oktmo;
    private String registrationDate;
    private String dissolutionDate;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "responseId")
    private Response response;

    @OneToOne(mappedBy = "responseIp", cascade = CascadeType.ALL)
    private Status status;

    public ResponseIp() {
    }

    public Long getId() {
        return id;
    }

    public String getFio() {
        return fio;
    }

    public String getOkpo() {
        return okpo;
    }

    public String getOkato() {
        return okato;
    }

    public String getOkfs() {
        return okfs;
    }

    public String getOkogu() {
        return okogu;
    }

    public String getOkopf() {
        return okopf;
    }

    public String getOpf() {
        return opf;
    }

    public String getOktmo() {
        return oktmo;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public String getDissolutionDate() {
        return dissolutionDate;
    }

    public Response getResponse() {
        return response;
    }

    public Status getStatus() {
        return status;
    }
}

package com.example.akbarsintegrationservice.response_models.response_ip;

import javax.persistence.*;

@Entity(name = "StatusIp")
@Table(name = "OUT_KF_RESPONSE_IP_STATUS")
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "responseIpId")
    private ResponseIp responseIp;

    private String statusString;

    @Column(nullable = true)
    private Boolean dissolving;
    @Column(nullable = true)
    private Boolean dissolved;
    private String date;

    public Status() {
    }

    public Long getId() {
        return id;
    }

    public ResponseIp getResponseIp() {
        return responseIp;
    }

    public String getStatusString() {
        return statusString;
    }

    public Boolean isDissolving() {
        return dissolving;
    }

    public Boolean isDissolved() {
        return dissolved;
    }

    public String getDate() {
        return date;
    }
}

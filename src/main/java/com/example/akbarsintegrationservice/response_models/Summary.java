package com.example.akbarsintegrationservice.response_models;

import javax.persistence.*;

@Entity
@Table(name = "OUT_KF_RESPONSE_BRIEF_REPORT_SUMMARY")
public class Summary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "briefReportId")
    private BriefReport briefReport;

    @Column(nullable = true)
    private Boolean greenStatements;
    @Column(nullable = true)
    private Boolean yellowStatements;
    @Column(nullable = true)
    private Boolean redStatements;

    public Summary() {
    }

    public Long getId() {
        return id;
    }

    public BriefReport getBriefReport() {
        return briefReport;
    }

    public Boolean isGreenStatements() {
        return greenStatements;
    }

    public Boolean isYellowStatements() {
        return yellowStatements;
    }

    public Boolean isRedStatements() {
        return redStatements;
    }
}

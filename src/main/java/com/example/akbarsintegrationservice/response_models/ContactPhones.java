package com.example.akbarsintegrationservice.response_models;

import org.springframework.lang.Nullable;

import javax.persistence.*;

@Entity
@Table(name = "OUT_KF_RESPONSE_BRIEF_REPORT")
public class ContactPhones {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(mappedBy = "briefReport", cascade = CascadeType.ALL)
    private Response response;

    @Column(name = "count_phones", nullable = true)
    private int count;

    public ContactPhones() {
    }

    public Long getId() {
        return id;
    }

    public Response getResponse() {
        return response;
    }

    public int getCount() {
        return count;
    }
}

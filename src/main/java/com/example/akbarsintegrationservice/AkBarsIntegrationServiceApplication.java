package com.example.akbarsintegrationservice;

import com.example.akbarsintegrationservice.swagger.SpringFoxConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({SpringFoxConfig.class})
public class AkBarsIntegrationServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AkBarsIntegrationServiceApplication.class, args);
    }

}
